use std::{
    collections::VecDeque,
    fs::File,
    io::{Read, Write},
};

use nom::{
    bytes::complete::{tag, take},
    error::{context, VerboseError},
    multi::{count, many1},
    number::complete::{le_f32, le_i32, le_i8},
    Finish,
};
use tracing::*;

/// Converts md2 to obj
fn main() {
    tracing_subscriber::fmt::init();
    let md2_filename = std::env::args().nth(1).expect("Filename to convert");

    let model_bytes = {
        let mut buf = Vec::new();
        let mut f = File::open(md2_filename).unwrap();
        f.read_to_end(&mut buf).unwrap();
        buf
    };

    let result = Md2Model::parse(&model_bytes).finish();
    match result {
        Err(e) => {
            println!("Failed to parse");
            println!("Errors: ");
            for (_slice, e) in &e.errors {
                println!("- {e:?}");
            }
            if let Some((slice, _e)) = e.errors.first() {
                println!("Data: {}", pretty_hex::pretty_hex(slice))
            }
        }
        Ok((_remainder, model)) => model.write_obj().unwrap(),
    }
}

#[derive(Debug)]
enum GlMode {
    Strip,
    Fan,
}

#[derive(Debug)]
struct GlCommand {
    mode: GlMode,
    vertices: Vec<usize>,
}

impl GlCommand {
    fn parse(i: &[u8]) -> nom::IResult<&[u8], GlCommand, VerboseError<&[u8]>> {
        let (i, command_num) = le_i32(i)?;
        let mode = if command_num > 0 {
            GlMode::Strip
        } else {
            GlMode::Fan
        };

        let parse_gl_command_vertex_index = |i| {
            let (i, _s) = le_f32(i)?;
            let (i, _t) = le_f32(i)?;
            let (i, vertex_index) = le_i32(i)?;
            Ok((i, vertex_index as usize))
        };

        let (i, vertices) = count(
            parse_gl_command_vertex_index,
            command_num.unsigned_abs() as usize,
        )(i)?;

        Ok((i, GlCommand { mode, vertices }))
    }
}

#[derive(Debug)]
struct Frame {
    vertices: Vec<Vertex>,
    scale: [f32; 3],
}

impl Frame {
    fn parse(i: &[u8], vertex_count: usize) -> nom::IResult<&[u8], Frame, VerboseError<&[u8]>> {
        let (i, scale) = context("getting scale", count(le_f32, 3))(i)?;
        let (i, _translate) = count(le_f32, 3)(i)?;
        let (i, _name) = take(16_u8)(i)?;
        let (i, vertices) = context("parsing vertices", count(Vertex::parse, vertex_count))(i)?;

        let frame = Frame {
            vertices,
            scale: scale.try_into().unwrap(),
        };

        Ok((i, frame))
    }
}

#[derive(Debug)]
struct Vertex {
    coords: [u8; 3],
    _light_normal_index: u8,
}

impl Vertex {
    fn parse(i: &[u8]) -> nom::IResult<&[u8], Vertex, VerboseError<&[u8]>> {
        let (i, coords) = take(3_u8)(i)?;
        let (i, light_normal_index) = le_i8(i)?;
        Ok((
            i,
            Vertex {
                coords: coords.try_into().unwrap(),
                _light_normal_index: light_normal_index as u8,
            },
        ))
    }
}

#[derive(Debug)]
struct Md2Model {
    frames: Vec<Frame>,
    gl_commands: Vec<GlCommand>,
}

impl Md2Model {
    fn parse(model_bytes: &[u8]) -> nom::IResult<&[u8], Md2Model, VerboseError<&[u8]>> {
        let i = model_bytes;
        let (i, _magic) = context("Reading magic value", tag(b"IDP2"))(i)?;
        let (i, version) = le_i32(i)?;

        if version != 8 {
            warn!("Unknown version {version}")
        }

        let (i, _skin_width) = le_i32(i)?;
        let (i, _skin_height) = le_i32(i)?;

        let (i, _frame_size) = le_i32(i)?;

        let (i, _num_skins) = le_i32(i)?;
        let (i, num_vertices) = le_i32(i)?;
        let (i, _num_tex_coords) = le_i32(i)?;
        let (i, _num_triangles) = le_i32(i)?;
        let (i, num_gl_commands) = le_i32(i)?;
        let (i, num_frames) = le_i32(i)?;

        let (i, _offset_skins) = le_i32(i)?;
        let (i, _offset_tex_coords) = le_i32(i)?;
        let (i, _offset_triangles) = le_i32(i)?;
        let (i, _offset_frames) = le_i32(i)?;
        let (i, offset_gl_commands) = le_i32(i)?;
        let (_, _offset_end) = le_i32(i)?;

        let frames_slice = &model_bytes[(_offset_frames as usize)..];
        let (_, frames) = context(
            "Parsing frame",
            count(
                |i| Frame::parse(i, num_vertices as usize),
                num_frames.try_into().unwrap(),
            ),
        )(frames_slice)?;

        let gl_commands_slice = &model_bytes
            [(offset_gl_commands as usize)..(offset_gl_commands + num_gl_commands * 4) as usize];

        let (_, mut gl_commands) = many1(GlCommand::parse)(gl_commands_slice)?;
        gl_commands.retain(|c| !c.vertices.is_empty());
        Ok((
            &model_bytes[(_offset_end as usize)..],
            Md2Model {
                frames,
                gl_commands,
            },
        ))
    }

    fn write_obj(&self) -> anyhow::Result<()> {
        let mut f = File::create("model.obj")?;
        let frame = &self.frames[0];

        let mut vert = 0;
        let mut faces = 0;

        for vertex in &frame.vertices {
            let [x, y, z] = vertex.coords;
            let x = x as f32 / 256.0 * frame.scale[0];
            let y = y as f32 / 256.0 * frame.scale[1];
            let z = z as f32 / 256.0 * frame.scale[2];
            writeln!(f, "v {x} {y} {z}")?;
            vert += 1;
        }
        for command in &self.gl_commands {
            if command.vertices.len() < 3 {
                warn!("GlCommand with less than 3 vertices");
                continue;
            }
            match command.mode {
                GlMode::Strip => {
                    let mut points = VecDeque::new();
                    points.extend(&command.vertices[..2]);

                    for v_index in &command.vertices[2..] {
                        points.push_back(v_index);
                        writeln!(f, "f {} {} {}", points[0] + 1, points[1] + 1, points[2] + 1)?;
                        faces += 1;
                        points.pop_front();
                    }
                }
                GlMode::Fan => {
                    let hub = command.vertices[0];
                    let mut prev = command.vertices[1];

                    for v_index in &command.vertices[2..] {
                        writeln!(f, "f {} {} {}", hub + 1, prev + 1, v_index + 1)?;
                        faces += 1;
                        prev = *v_index;
                    }
                }
            }
        }
        info!("Written model.obj with {vert} vertices and {faces} faces");

        Ok(())
    }
}
